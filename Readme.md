# Apptek Transcribe Streaming Service Client For Apptek Cloud

Assuming Node.js is installed and dockerized streaming-asr is available.

1. Requires Apptek license key.
2. Install required packages (pacakge.json)
3. Place .env file with the options provided at project root or the options provided can be chained with run command.
4. If you provide .env file run 

    `node streaming.js`
    
    or 
    
    `encoding=< encoding > sample_rate=< Sampling Rate > audio_file=< Path to Audio File > node streaming.js`

ENV File:

    grpc_url=< asr streaming docker instance IP/DNS and port >
    encoding=< encoding >,
    sample_rate=< Sampling Rate >,
    audio_file=< Path to Audio File >

Available Options:

    8000Hz Audio:

        Sampling Rate : 8000
        encoding : 
            PCM_16bit_SI_MONO = 0;
            FLAC = 1;
            OGG_OPUS = 2;

