function transcribeClient() {

    const grpc = require('grpc');
    const protoLoader = require('@grpc/proto-loader');
    const path = require('path');
    const grpcUrl = process.env.grpc_url || 'localhost:11000';
    
    // .proto file location 
    const PROTO_PATH = path.join(__dirname, './apptek.proto');
    
    // redaing config
    var audio_config = {
        encoding : process.env.encoding || 0,
        sample_rate : process.env.sample_rate || 8000
    }

    var client, audioCall;

    const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
    const packageDescriptor = grpc.loadPackageDefinition(packageDefinition);
    const mtp_stream = packageDescriptor.mtp_stream;

    // const credentials = grpc.credentials.createSsl(fs.readFileSync(CERT_PATH));
    client = new mtp_stream.MtpStreamAsr(grpcUrl, grpc.credentials.createInsecure());
    
    this.streamAudio = function (data, cb) {
        var messageAudio = {
            // oneof_request : {
                audio: data.audio
            // }
        }
        if (audioCall) {
            audioCall.write(messageAudio);
        }else{
            var configuration = {
                encoding : parseInt(audio_config.encoding),
                sample_rate : parseInt(audio_config.sample_rate),
                segmentation_enabled: true,
	            speaker_change_enabled: true,
	            segment_end_signal_enabled: true,	
	            confidence_enabled: true,
	            custom_lexicon_config: null, // custom dictionary is disabled when it is null	
	            // drop_audio: {
                //     overflow_in_seconds: audio_config.overflow_in_seconds,
	            //     underflow_in_seconds: audio_config.underflow_in_seconds
                // }
            }
            var config = {
                // oneof_request : {
                    config: configuration
                // }
            }
            audioCall = client.StreamAudio();
            audioCall.on('data', function (data) {
                cb(null, data);
            });
            audioCall.on('end', function () {
                console.log("Connection Closed");
            });
            audioCall.on('error', function (e) {
                console.log(e);
                cb(e);
            });
            audioCall.write(config);
            audioCall.write(messageAudio);
        }
    };

    this.endAudioCall = function () {
        if(audioCall){
            audioCall.end();
            audioCall = '';
        }
    }
}

module.exports = transcribeClient;