"use strict";

require('dotenv').config({path: './.env'})
const fs = require('fs');

// getting instance of a transcribe service.
const transcribeService = new(require("./transcription_service.js"))();
var audioBuffer = [];

// adding a simple wait just to simulate streaming use case
function wait(milleseconds) {
    return new Promise(resolve => setTimeout(resolve, milleseconds))
}

// sending audio packets 
async function sendAudioToBot() {
    for (let i = 0; i < audioBuffer.length; i++) {
        // fake delay of 1sec to simulate streaming.
        await wait(1000);
        
        if(i == 0 ){
            console.log('config sent');
        }else if(i == 1){
            console.log("streaming Audio from buffer");
        }else if(i >= audioBuffer.length){
            console.log("Audio streaming complete");
        }

        var data = {
            audio: audioBuffer[i]
        }
        // sending audio and 
        transcribeService.streamAudio(data, function(err, data){
            if(err){
                console.log(err);
            }else{
                // unncomment if you want to see all the messages.
                // console.log(data)
                if(data.oneof_response && data.oneof_response == 'transcription'){
                    if(data.transcription.orth){
                        console.log('" ',data.transcription.orth, ' "');
                    }
                }
            }
        });
    }
};

function readFile(){
    var fileName = process.env.audio_file;
    var readStream = fs.createReadStream(fileName);
    readStream.on('data', function (chunk) {
        // console.log(chunk);
        audioBuffer.push(chunk);
    });
    readStream.on('end', function(){
        console.log('File read to buffer');
    });
    readStream.on('error', function(err) {
        console.log(err);
    });
}

readFile();

setTimeout(() => {
    sendAudioToBot();
}, 1000);
  
// To keep node process from terminating. 
// Press ctrl + c to exit.
setInterval((function() {
    return;
}), 10000);
